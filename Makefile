.PHONY: help pdfs

BUILD_FOLDER = build

help:			## Shows this message.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

compile_sectorfiles:
	rm -f ${BUILD_FOLDER}/sectorfile_lppc.pdf
	latexmk -pdf sectorfile/lppc.tex && \
	mv lppc.pdf ${BUILD_FOLDER}/sectorfile_lppc.pdf \

compile_regulations:
	rm -f ${BUILD_FOLDER}/estatutos-pt.pdf
	rm -f ${BUILD_FOLDER}/regulamento-pt.pdf
	latexmk -pdf regulations/estatutos-pt.tex && \
	latexmk -pdf regulations/regulamento-pt.tex && \
	mv estatutos-pt.pdf ${BUILD_FOLDER}/estatutos-pt.pdf && \
	mv regulamento-pt.pdf ${BUILD_FOLDER}/regulamento-pt.pdf \

compile_lppc:
	rm -f ${BUILD_FOLDER}/lppc_vfr_waypoints.pdf
	latexmk -pdf lppc/vfr_waypoints/main.tex && \
	mv main.pdf ${BUILD_FOLDER}/lppc_vfr_waypoints.pdf \

compile: compile_sectorfiles compile_lppc
